#ifndef __REDIRECT_H_
#define __REDIRECT_H_

#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include "usart.h"

int _isatty(int fd);
int _write(int fd, char* ptr, int len);
int _close(int fd);
int _lseek(int fd, int ptr, int dir);
int _read(int fd, char* ptr, int len);
int _fstat(int fd, struct stat* st);

#endif