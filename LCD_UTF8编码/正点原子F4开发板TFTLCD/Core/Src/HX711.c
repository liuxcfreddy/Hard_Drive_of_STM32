/*
 * HX711.c
 *
 *  Created on: Mar 23, 2023
 *      Author: liuxc
 */
#include "hx711.h"

uint32_t read(void)
{

	unsigned long Count;
	unsigned char i;
	HAL_GPIO_WritePin(SCK_GPIO_Port, SCK_Pin, 0);
	Count = 0;
	while (HAL_GPIO_ReadPin(DT_GPIO_Port, DT_Pin))
		;
	for (i = 0; i < 24; i++)
	{
		HAL_GPIO_WritePin(SCK_GPIO_Port, SCK_Pin, 1);
		Count = Count << 1;
		HAL_GPIO_WritePin(SCK_GPIO_Port, SCK_Pin, 0);
		if (HAL_GPIO_ReadPin(DT_GPIO_Port, DT_Pin))
			Count++;
	}
	HAL_GPIO_WritePin(SCK_GPIO_Port, SCK_Pin, 1);
	Count = Count ^ 0x800000;
	HAL_GPIO_WritePin(SCK_GPIO_Port, SCK_Pin, 0);
	return (Count);
}
