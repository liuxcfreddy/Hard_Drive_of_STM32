/*
 * HX711.h
 *
 *  Created on: Mar 23, 2023
 *      Author: liuxc
 */

#ifndef INC_HX711_H_
#define INC_HX711_H_

#include "main.h"

uint32_t read(void);

#endif /* INC_HX711_H_ */
