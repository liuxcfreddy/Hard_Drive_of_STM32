# STM32基于Cube IDE的硬件驱动

## 介绍

> 增加了ws2812B的SPI 和 DMA 驱动
> 
> 增加了vofa私有协议的驱动
> 
> 增加了NRF24驱动（SPI协议）
> 
> 增加了Flash储存芯片驱动（SPI协议）
> 
> 使用STM32的硬件IIC进行于0.96寸OLED的通信传输
> 
> 使用STM32的硬件SPI进行W25Qxx型号的Flash读写
> 
> 使用printf重定向进行串口打印，也可以重定向到其他任何的传输函数中去，甚至到显示外设之中均可
> 
> 增加了MPU6050的DMP解算库

### 软件架构

STM32 Cube IDE

STM32 CUBE MX

### SDK

HAL


