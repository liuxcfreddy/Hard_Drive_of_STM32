/*
 * @Author: error: error: git config user.name & please set dead value or install git && error: git config user.email & please set dead value or install git & please set dead value or install git
 * @Date: 2023-03-13 20:45:34
 * @LastEditors: error: error: git config user.name & please set dead value or install git && error: git config user.email & please set dead value or install git & please set dead value or install git
 * @LastEditTime: 2023-03-14 19:08:12
 * @FilePath: \遥控调试\HARDWARE\sbus\sbus.c
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
#include "sbus.h"
#include "main.h"
#include "usart.h"



SBUS_CH_Struct SBUS_CH; 						//sbus通道值结构体定义（需要外部声明使用）
uint8_t  USART_RX_BUF_SBUS[USART_REC_LEN_SBUS]; //接收缓冲区字节定义（需要外部声明使用）
Typedef_Conver Remote_Control;//定义摇杆量

/*
 * @ brief SBUS初始化参数程序最开始调用一次，采用DMA方式给接受
 * @ param 输入你选择的串口号
 * 			huart1
 * 			huart2
 * 	@docx 程序初始化的时候
 */
void Subs_Init(UART_HandleTypeDef* USARTx,uint8_t* USART_RX_BUF_SBUS)
{
	 while(HAL_UART_Receive_DMA(USARTx, USART_RX_BUF_SBUS, 25)!=HAL_BUSY);
}
/*
 * sbus通道数据转换函数，内部调用
 * 被sbus_cheek();依赖
 * buf:获取的字节数据
 * SBUS_CH 输出解析后的数据
 */
uint8_t updata_sbus(uint8_t *buf,SBUS_CH_Struct* SBUS_CH)
{
    if (buf[24] == 0) 
    {
        SBUS_CH->CH1 = ((int16_t)buf[ 1] >> 0 | ((int16_t)buf[ 2] << 8 ))                            & 0x07FF;
        SBUS_CH->CH2 = ((int16_t)buf[ 2] >> 3 | ((int16_t)buf[ 3] << 5 ))                            & 0x07FF;  //
        SBUS_CH->CH3 = ((int16_t)buf[ 3] >> 6 | ((int16_t)buf[ 4] << 2 ) | (int16_t)buf[ 5] << 10 )  & 0x07FF; //左手上下
        SBUS_CH->CH4 = ((int16_t)buf[ 5] >> 1 | ((int16_t)buf[ 6] << 7 ))                            & 0x07FF; //左手左右
        SBUS_CH->CH5 = ((int16_t)buf[ 6] >> 4 | ((int16_t)buf[ 7] << 4 ))                            & 0x07FF; //SWA -5
        SBUS_CH->CH6 = ((int16_t)buf[ 7] >> 7 | ((int16_t)buf[ 8] << 1 ) | (int16_t)buf[9] << 9 )    & 0x07FF; //SWA -6
        SBUS_CH->CH7 = ((int16_t)buf[ 9] >> 2 | ((int16_t)buf[10] << 6 ))                            & 0x07FF; //SWA -7
        SBUS_CH->CH8 = ((int16_t)buf[10] >> 5 | ((int16_t)buf[11] << 3 ))                            & 0x07FF; //SWA -8

        SBUS_CH->CH9 = ((int16_t)buf[12] << 0 | ((int16_t)buf[13] << 8 ))                            & 0x07FF;
        SBUS_CH->CH10 = ((int16_t)buf[13] >> 3 | ((int16_t)buf[14] << 5 ))                           & 0x07FF;
        SBUS_CH->CH11 = ((int16_t)buf[14] >> 6 | ((int16_t)buf[15] << 2 ) | (int16_t)buf[16] << 10 ) & 0x07FF;
        SBUS_CH->CH12 = ((int16_t)buf[16] >> 1 | ((int16_t)buf[17] << 7 ))                           & 0x07FF;
        SBUS_CH->CH13 = ((int16_t)buf[17] >> 4 | ((int16_t)buf[18] << 4 ))                           & 0x07FF;
        SBUS_CH->CH14 = ((int16_t)buf[18] >> 7 | ((int16_t)buf[19] << 1 ) | (int16_t)buf[20] << 9 )  & 0x07FF;
        SBUS_CH->CH15 = ((int16_t)buf[20] >> 2 | ((int16_t)buf[21] << 6 ))                           & 0x07FF;
        SBUS_CH->CH16 = ((int16_t)buf[21] >> 5 | ((int16_t)buf[22] << 3 ))                           & 0x07FF;
        SBUS_CH->ConnectState = 1;
    }

    return HAL_OK;
}
/**
 * 遥控器通道值初始化默认值
 * 参数：遥控器通道值结构体
 * 依赖：被 Sbus_Cheek();使用
 * note: 如果失控则给遥控器的值 赋"0"以防乱飞;
 * 返回值：无
 */
void Sbus_CHx_Init(SBUS_CH_Struct * SBUS_CH)
{
	SBUS_CH->CH1=990;
	SBUS_CH->CH2=990;
	SBUS_CH->CH3=200;
	SBUS_CH->CH4=990;
	SBUS_CH->CH5=Toggle_switch_Down;
	SBUS_CH->CH6=Toggle_switch_Down;
	SBUS_CH->CH7=Toggle_switch_Down;
	SBUS_CH->CH8=Toggle_switch_Down;
}
/****数据检查函数****/
/*
*功能：检查sbus传输数据是否正确，同时更新通道值的全局变量，需要频繁调用
*参数： 	USART_RX_BUF_SBUS: 接收的Sbus信号的数组地址
*     	SBUS_CH:输出解析好的通道值结构体地址
*返回值：无 输入一个定义好的结构体就可以读取数值了
*/
void Sbus_Cheek(uint8_t * USART_RX_BUF_SBUS ,SBUS_CH_Struct * SBUS_CH)
{
    	//SBUS通信协议解析
        if (USART_RX_BUF_SBUS[0] == 0x0F &&
        	USART_RX_BUF_SBUS[24] == 0x00 &&
			(USART_RX_BUF_SBUS[23] & 0x08) == 0 )	//接受完一帧数据,最后一个判断准则就是判断第五个数据位（失控标志位）是否正常
        {
        	while(updata_sbus(USART_RX_BUF_SBUS,SBUS_CH));
        	for(uint8_t i=0;i<25;i++)
        	{
        		USART_RX_BUF_SBUS[i]=0;
        	}
        }
        else
           {
               SBUS_CH->ConnectState = 0;//遥控器断开
               Sbus_CHx_Init(SBUS_CH);
           }

}
/*
 * @name:Numeric_Conversion;
 * @brief :Convert the remote control value
 * to a 0~100 Floating-point arithmetic number
 */
void Numeric_Conversion(SBUS_CH_Struct* SBUS_CH,Typedef_Conver * Remote_Control)
{
	Remote_Control->CH1=(SBUS_CH->CH1-430)/11.2;
	Remote_Control->CH2=(SBUS_CH->CH2-430)/11.2;
	Remote_Control->CH3=(SBUS_CH->CH3-200)/16;
	Remote_Control->CH4=(SBUS_CH->CH4-430)/11.2;
}
