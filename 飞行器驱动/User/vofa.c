/*
 * vofa.c
 *
 *  Created on: 2023年7月1日
 *      Author: liuxc
 */
#include "vofa.h"
#include "usart.h"

void VOFA_SEND(UP_Soft_TX_DataBuff *framedata)
{
	HAL_UART_Transmit(&huart1,(uint8_t *)framedata->fdata,sizeof(framedata->fdata), 0xffff);
	HAL_UART_Transmit(&huart1,(uint8_t *)framedata->tail,4,0xffff);
}
