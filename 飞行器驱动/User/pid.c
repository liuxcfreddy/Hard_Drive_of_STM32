/*
 * pid.c
 *
 *  Created on: 2023年4月1日
 *      Author: liuxc
 */
#include "pid.h"
#include "main.h"
//#include "mpu9250.h"
#include "flyctrl.h"
/*
 * PID初始化控制器
 * @prgam	KP
 * 			KI
 * 			KD
 * 			setpoint 期望值
 */
void PIDController_init(PIDController *pid, double Kp, double Ki, double Kd, float setpoint)
{
	pid->Kp = Kp;
	pid->Ki = Ki;
	pid->Kd = Kd;
	pid->setpoint = setpoint;
	pid->error_sum = 0;
	pid->last_error = 0;
}

/*@ brief:计算PID控制器输出
 *@ param:pid PID参数结构体
 *@ param:input 姿态传感器数据
 *@ param:dt pid控制周期，微分时间判断,可以理解为这个值代表着两次PID控制间隙中，我们的error值经过了多久的时间)
 *@ 这个时间可以简单的理解为我们刷新了几次error数据;如果这个值越大，响应过程中将会约激烈
 *@ note: 这是一个增量式PID控制函数；
 */
float PIDController_compute(PIDController *pid, float input, float dt)
{
	// 计算误差
	float error = pid->setpoint - input;

	// 计算误差累计值
	pid->error_sum += error * dt;

	// 计算误差变化率
	float error_delta = (error - pid->last_error) / dt;

	// 计算PID控制器输出
	float output = 	  pid->Kp * error
					+ pid->Ki * pid->error_sum
					+ pid->Kd * error_delta;

	// 保存上一次误差值
	pid->last_error = error;

	return output;
}

/**
 *@brief:位置式PID控制函数;
 *@ pram:pid PID控股结构体;
 *@ parm:error 当前量与期望值之间的误差;
 */
float PIDUpdate(PIDObject* pid, const float error)
{
	float output;

	pid->error = error;

	pid->integ += pid->error * pid->dt;
	if (pid->integ > pid->iLimit)
	{
		pid->integ = pid->iLimit;
	}
	else if (pid->integ < pid->iLimitLow)
	{
		pid->integ = pid->iLimitLow;
	}

	pid->deriv = (pid->error - pid->prevError) / pid->dt;

	pid->outP = pid->kp * pid->error;
	pid->outI = pid->ki * pid->integ;
	pid->outD = pid->kd * pid->deriv;

	output = pid->outP + pid->outI + pid->outD;

	pid->prevError = pid->error;

	return output;
}

