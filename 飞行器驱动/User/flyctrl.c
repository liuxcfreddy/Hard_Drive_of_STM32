/*
 * flycrtl.c
 *
 *  Created on: 2023年3月20日
 *      Author: Administrator
 */
#include "main.h"
#include "sbus.h"
#include "flyctrl.h"
#include "tim.h"



#include "pid.h"
/*
 * PWM输出初始化开启
 */
void PWM_OUT_Init()
{
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_4);
}
/*
 * PWM占空比修改内部用
 * 主要修改的是脉冲宽度
 * 被占空比修改所依赖
 */
void PWM_SETDUTY(PWM_DutyData *Duty)
{
	__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, (uint32_t)Duty->MotoA);
	__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_2, (uint32_t)Duty->MotoB);
	__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_3, (uint32_t)Duty->SeverA);
	__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, (uint32_t)Duty->SeverB);
}

/*
 * 用于修改PWM的占空比
 * 传入参数为百分比
 */
void FLYING_Set_Duty(float MotoA, float MotoB,
		float SeverA,float SeverB)
{
	if (MotoA >= 0 && MotoA <= 100
		&& MotoB >= 0 && MotoB <= 100
		&& SeverA >= 0 && SeverA <= 100
		&& SeverB >= 0 && SeverB <= 100)
	{
		PWM_DutyData PWM_DTUY_INIT_STRUCT;//内部初始化变量用于向结构体传参

		PWM_DTUY_INIT_STRUCT.MotoA = (10 * MotoA + 1000);
		PWM_DTUY_INIT_STRUCT.MotoB = (10 * MotoB + 1000);
		PWM_DTUY_INIT_STRUCT.SeverA = (20 * SeverA + 500);
		PWM_DTUY_INIT_STRUCT.SeverB = (20 * SeverB + 500);

		PWM_SETDUTY(&PWM_DTUY_INIT_STRUCT);//传参结束
	}
}

/*
 * 用于进行电调舵机的动作校准；
 * 初始化调用一次
 * 脉宽为1000~2000之间；
 */
void FLYSTART()
{
	PWM_DutyData PWM_DTUY_INIT_STRUCT;
	uint16_t i;
	for (i = 1000; i < 2000; i++)
	{
		PWM_DTUY_INIT_STRUCT.MotoA = i;
		PWM_DTUY_INIT_STRUCT.MotoB = i;
		PWM_DTUY_INIT_STRUCT.SeverA = i;
		PWM_DTUY_INIT_STRUCT.SeverB = i;
		PWM_SETDUTY(&PWM_DTUY_INIT_STRUCT);
		if (i == 1000)
		{
			HAL_Delay(1000); //低杆确认
		}
		if (i == 2000)
		{
			HAL_Delay(1000); //高杆确认
		}
	}
	/*恢复初值*/
	PWM_DTUY_INIT_STRUCT.MotoA = 1500;
	PWM_DTUY_INIT_STRUCT.MotoB = 1500;
	PWM_DTUY_INIT_STRUCT.SeverA = 1500;
	PWM_DTUY_INIT_STRUCT.SeverB = 1500;
	PWM_SETDUTY(&PWM_DTUY_INIT_STRUCT);
}
