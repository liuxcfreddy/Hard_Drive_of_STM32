/*
 * flyctrl.h
 *
 *  Created on: 2023年3月20日
 *      Author: Administrator
 */

#ifndef INC_FLYCTRL_H_
#define INC_FLYCTRL_H_

#include "sbus.h"

extern uint8_t USART_RX_BUF_SBUS[USART_REC_LEN_SBUS];


//输出功率结构体
typedef struct{
	float MotoA;
	float MotoB;
	float SeverA;
	float SeverB;
}TypePowerOut;

//PWM占空比结构体
typedef struct
{
	float MotoA;
	float MotoB;
	float SeverA;
	float SeverB;

}PWM_DutyData;


/*
 * 电调参数修改函数区
 */
void PWM_OUT_Init();
void PWM_SETDUTY(PWM_DutyData * Duty);
void FLYSTART();
void FLYING_Set_Duty(float MotoA,float MotoB,float SeverA,float SeverB);





#endif /* INC_FLYCTRL_H_ */
