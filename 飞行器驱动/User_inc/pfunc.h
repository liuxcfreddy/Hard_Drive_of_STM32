/*
 * pfunc.h
 *
 *  Created on: Feb 15, 2023
 *      Author: liuxc
 *      Doc:printf()函数重定向
 */

#ifndef INC_PFUNC_H_
#define INC_PFUNC_H_


#include "main.h"
#include "usart.h"
#include <stdio.h>


//int fputc(int ch, FILE *f)
//{
// HAL_UART_Transmit(&huart1, (uint8_t *)&ch, 1, 0xFFFF);//更具实际情况更改驱动
//         return (ch);
//}
//struct __FILE
//{
//        int handle;
//};
//FILE __stdout;
//#if defined(__GNUC__)
//int _write(int fd, char * ptr, int len)
//{
//  HAL_UART_Transmit(&huart1, (uint8_t *) ptr, len, HAL_MAX_DELAY);
//  return len;
//}
//#endif

#ifdef __GNUC__									//串口重定向
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif
PUTCHAR_PROTOTYPE
{
    HAL_UART_Transmit(&huart1 , (uint8_t *)&ch, 1, 0xFFFF);
    return ch;
}
#endif /* INC_PFUNC_H_ */
