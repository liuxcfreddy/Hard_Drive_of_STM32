/*
 * pid.h
 *
 *  Created on: 2023年4月1日
 *      Author: liuxc
 */

#ifndef INC_PID_H_
#define INC_PID_H_

#include "main.h"

// 定义增量式PID控制器结构体
typedef struct {
    float Kp;  // 比例系数
    float Ki;  // 积分系数
    float Kd;  // 微分系数
    float setpoint;  // 目标值
    float error_sum;  // 误差累计值
    float last_error;  // 上一次误差值
} PIDController;

//定义位置式PID控制器结构体
typedef struct
{
	float desired;		//< set point 设定值

	float error;        //< error 误差值
	float prevError;    //< previous error 	上一次误差
	float integ;        //< integral 		积分值
	float deriv;        //< derivative 		微分值

	float kp;           //< proportional gain 比例增益 用户设定
	float ki;           //< integral gain 积分增益     用户设定
	float kd;           //< derivative gain 微分增益   用户设定

	float outP;         //< proportional output (debugging) //比例值输出缓冲
	float outI;         //< integral output (debugging)//积分值输出缓冲
	float outD;         //< derivative output (debugging)//微分值输出缓冲

	float iLimit;       //< integral limit 积分值极限上限 	用户设定
	float iLimitLow;    //< integral limit 积分值下限	 	用户设定
	float dt;           //< delta-time dt 间隔时间    	用户设定
} PIDObject;



//姿态中间算子
typedef struct{
	float Up_Down;
	float Go_Back;
	float Lift_Right;
	float Roll;
	float Pitch;
	float Yaw; //自旋算子PID输出量
}TypeOperator;


//输出比例算子
typedef struct{
	float Up_Down;
	float Go_Back;
	float Lift_Right;
	float Roll;
	float Pitch;
	float Yaw;
}TypeOperator_Pro;

/*
 * PID函数区域
 */
void PIDController_init(PIDController *pid, double Kp, double Ki, double Kd, float setpoint);
float PIDController_compute(PIDController *pid, float input, float dt);
float PIDUpdate(PIDObject* pid, const float error);

#endif /* INC_PID_H_ */
