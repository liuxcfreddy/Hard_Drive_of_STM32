/*
 * @Author: error: error: git config user.name & please set dead value or install git && error: git config user.email & please set dead value or install git & please set dead value or install git
 * @Date: 2023-03-13 20:45:34
 * @LastEditors: error: error: git config user.name & please set dead value or install git && error: git config user.email & please set dead value or install git & please set dead value or install git
 * @LastEditTime: 2023-03-14 19:08:30
 * @FilePath: \遥控调试\HARDWARE\sbus\sbus.h
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
#ifndef __SBUS_H
#define __SBUS_H
#include "stdio.h"
#include "main.h"

/**
 * 遥控器钮子开关对应值定义
 * */
#define Toggle_switch_Up 192
#define Toggle_switch_Mid 992
#define Toggle_switch_Down 1792
/*
**串口波特率100k
**数据位9
**偶校验
**无硬件流控制
**一位停止位
**发送 接收 模式都使用
*/
typedef struct //HT-8A当前接收机仅有8通道
{
	uint16_t CH1;//通道1数值
	uint16_t CH2;//通道2数值
	uint16_t CH3;//通道3数值
	uint16_t CH4;//通道4数值
	uint16_t CH5;//通道5数值
	uint16_t CH6;//通道6数值
    uint16_t CH7;//通道7数值
    uint16_t CH8;//通道8数值
    
    uint16_t CH9;//通道9数值
    uint16_t CH10;//通道10数值
    uint16_t CH11;//通道11数值
    uint16_t CH12;//通道12数值
    uint16_t CH13;//通道13数值
    uint16_t CH14;//通道14数值
    uint16_t CH15;//通道15数值
    uint16_t CH16;//通道16数值
    uint8_t ConnectState;//遥控器与接收器连接状态 0=未连接，1=正常连接
}SBUS_CH_Struct;

//摇杆百分比量 0~100;
typedef struct
{
	float CH1;
	float CH2;
	float CH3;
	float CH4;
}Typedef_Conver;

#define USART_REC_LEN_SBUS 25   //定义最大接收字节数 25;

uint8_t updata_sbus(uint8_t *buf,SBUS_CH_Struct* SBUS_CH); //数据解析

void Subs_Init(UART_HandleTypeDef* USARTx,uint8_t* USART_RX_BUF_SBUS);//初始化声明调用(调用一次即可)
void Sbus_Cheek(uint8_t * USART_RX_BUF_SBUS ,SBUS_CH_Struct * SBUS_CH);//讲数据进行解析 需要被循环调用
void Numeric_Conversion(SBUS_CH_Struct* SBUS_CH,Typedef_Conver * Remote_Control);//讲摇杆数据转换为百分比

#endif
