/*
 * WB2812.c
 *
 *  Created on: Jul 24, 2023
 *      Author: liuxc
 */

/************************************************************
Copyright (C), 2013-2020, XINZHIYIKONG.Co.,Ltd.
@FileName: WS2812B.c
@Author  : 糊读虫 QQ:570525287
@Version : 1.0
@Date    : 2020-12-25
@Description: WS2812B全彩LED灯驱动
@Function List:
@History    :
<author> <time> <version > <desc>

***********************************************************/

#include "ws2812.h"




void WS2812_SendPixel(uint8_t *  temp)
{
	 HAL_SPI_Transmit(&WS2812_IN, temp, 24, 1000);
}

void WS2812_SetColor(TypedefSetcolor * Matrix,uint8_t index,uint8_t R,uint8_t G,uint8_t B)
{
	for(uint8_t i=0;i<8;i++ )
	{
		Matrix->Matrix_data[index].Green[i] =(((G<<i)&0x80)?CODE1:CODE0);
		Matrix->Matrix_data[index].Red[i] 	= (((R<<i)&0x80)?CODE1:CODE0);
		Matrix->Matrix_data[index].Blue[i] =  (((B<<i)&0x80)?CODE1:CODE0);
	}
}

void WS2812_SendData(TypedefSetcolor * Matrix)
{
	uint8_t RES[280]={0};
	for(int i= 0;i<LED_Num;i++)
	{
		WS2812_SendPixel((uint8_t *)&Matrix->Matrix_data[i]);
		HAL_SPI_Transmit(&WS2812_IN, RES, 24, 1000);
	}

	//HAL_Delay(1);
}
