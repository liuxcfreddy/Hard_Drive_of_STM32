# 使用说明

WS2812的单线通信时钟非常的高所以常规的通信方式都不太方便，使用TIM则要配置时钟环路与DMA缓存，非常的麻烦，使用SPI的MOSI线路输出响应的数据位置是一个不错的办法，所以我们需要在配置页面将SPI配置为如下图所示：

![setup](https://gitee.com/liuxcfreddy/Hard_Drive_of_STM32/raw/master/WS2812/img/setup.png)

这里存在一个计算，SPI的速度要大概介于5Mbit/s~9Mbit/s之间，具体的计算我没有算，但是其他博主有计算过程，我也懒得深究，所以这里我就设置为了8Mbit/s,其余的设置按照我这个模式来就可以；

```c
//ws2812.h
#define LED_Num 16

#define CODE0 0xc0
#define CODE1 0xFC

#define WS2812_IN hspi1

typedef struct{
    uint8_t Green[8];
    uint8_t Red[8];
    uint8_t Blue[8];
}TypedefColor;

typedef struct{
    TypedefColor  Matrix_data[LED_Num];
}TypedefSetcolor;

void WS2812_SendPixel(uint8_t * temp);
void WS2812_SetColor(TypedefSetcolor * Matrix,uint8_t index,uint8_t R,uint8_t G,uint8_t B);
void WS2812_SendData(TypedefSetcolor * Matrix);
```

**LED_Num** 宏定义的为你这条链路上使用的LED灯的数量，一般说我们可以程控1024颗，但是，我也没试过;

使用的时候需要在主函数自己声明一个 **TypedefSetcolor** 的结构体。如下：

main.c

```c
 TypedefSetcolor WS2812_ALLData={
          .Matrix_data={0}
  };
   while (1)
  {
      for(int i =0 ;i<16;i++)
       {
          WS2812_SetColor(&WS2812_ALLData,i,15*j,0,128);
          WS2812_SendData(&WS2812_ALLData);
          j++;
          HAL_Delay(100);
       }
      j=0;
      HAL_Delay(1000);
      for(int i =0 ;i<16;i++)
       {
          WS2812_SetColor(&WS2812_ALLData,i,0,0,0);
       }
      WS2812_SendData(&WS2812_ALLData);

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
```

在main.c中的调用如上面所示：先用WS2812_SetColor()函数设置颜色到缓存区，然后统一用WS2812_SendData()函数一起发送；这里我是简单的使用16个灯做了一个流水渐变;

如果要使用HSV用来实现RGB流动效果可以自己添加HSV转换RGB色域的代码：

```c
//参数入参范围h(0~360),s(0~100),v(0~100),这里要注意，要把s,v缩放到0~1之间
//转换结果R(0~100),G(0~100),B(0~100)，如需转换到0~255，只需把后面的乘100改成乘255
void hsv_to_rgb(int h,int s,int v,float *R,float *G,float *B)
{
    float C = 0,X = 0,Y = 0,Z = 0;
    int i=0;
    float H=(float)(h);
    float S=(float)(s)/100.0;
    float V=(float)(v)/100.0;
    if(S == 0)
        *R = *G = *B = V;
    else
    {
        H = H/60;
        i = (int)H;
        C = H - i;

        X = V * (1 - S);
        Y = V * (1 - S*C);
        Z = V * (1 - S*(1-C));
        switch(i){
            case 0 : *R = V; *G = Z; *B = X; break;
            case 1 : *R = Y; *G = V; *B = X; break;
            case 2 : *R = X; *G = V; *B = Z; break;
            case 3 : *R = X; *G = Y; *B = V; break;
            case 4 : *R = Z; *G = X; *B = V; break;
            case 5 : *R = V; *G = X; *B = Y; break;
        }
    }
    *R = *R *100;
    *G = *G *100;
    *B = *B *100;
}
```

接驳办法可以自己嵌套一个打包函数将setcolor包含在其中
