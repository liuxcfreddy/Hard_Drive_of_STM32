/*
 * wb2812.h
 *
 *  Created on: Jul 24, 2023
 *      Author: liuxc
 */

#ifndef INC_WS2812_H_
#define INC_WS2812_H_

#include "main.h"
#include "spi.h"

#define LED_Num 16

#define CODE0 0xc0
#define CODE1 0xFC

#define WS2812_IN hspi1

typedef struct{
	uint8_t Green[8];
	uint8_t Red[8];
	uint8_t Blue[8];
}TypedefColor;

typedef struct{
	TypedefColor  Matrix_data[LED_Num];
}TypedefSetcolor;



void WS2812_SendPixel(uint8_t * temp);
void WS2812_SetColor(TypedefSetcolor * Matrix,uint8_t index,uint8_t R,uint8_t G,uint8_t B);
void WS2812_SendData(TypedefSetcolor * Matrix);
#endif /* INC_WB2812_H_ */
