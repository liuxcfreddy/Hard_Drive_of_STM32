#ifndef _IIC_H_
#define _IIC_H_
#include "main.h"
#include "i2c.h"
#define MPU6050_IIC_Hard_No hi2c2
/*
 * IIC底层驱动接口，实现一下接口即可获得一样的效果
 * 需要注意的是，你需要在初始化时候声明HAL库的i2c
 */

uint8_t IICwriteBits(uint8_t dev,uint8_t reg,uint8_t bitStart,uint8_t length,uint8_t data);
uint8_t IICwriteBit(uint8_t dev,uint8_t reg,uint8_t bitNum,uint8_t data);

int i2cWrite(uint8_t addr, uint8_t reg, uint8_t len, uint8_t *data);
int i2cRead(uint8_t addr, uint8_t reg, uint8_t len, uint8_t *buf);


#endif
















