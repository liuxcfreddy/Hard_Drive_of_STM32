#include <iic.h>
#include "i2c.h"





/**************************实现函数********************************************
*函数原型:		bool i2cWrite(uint8_t addr, uint8_t reg, uint8_t data)
*功　　能:		
*******************************************************************************/
int i2cWrite(uint8_t addr, uint8_t reg, uint8_t len, uint8_t *data)
{
     HAL_I2C_Mem_Write(&MPU6050_IIC_Hard_No, addr, reg, I2C_MEMADD_SIZE_8BIT, data, len, 0xff);
     return HAL_OK;
}
/**************************实现函数********************************************
*函数原型:		bool i2cWrite(uint8_t addr, uint8_t reg, uint8_t data)
*功　　能:		
*******************************************************************************/
int i2cRead(uint8_t addr, uint8_t reg, uint8_t len, uint8_t *buf)
{
 return HAL_I2C_Mem_Read(&MPU6050_IIC_Hard_No, addr, reg, I2C_MEMADD_SIZE_8BIT, buf, len, 0xff);
 return HAL_OK;
}


uint8_t IICwriteBits(uint8_t dev,uint8_t reg,uint8_t bitStart,uint8_t length,uint8_t data)
{

    uint8_t b;
    if (HAL_I2C_Mem_Read(&MPU6050_IIC_Hard_No, dev, reg, I2C_MEMADD_SIZE_8BIT, &b, 1, 0xff)==HAL_OK) {
        uint8_t mask = (0xFF << (bitStart + 1)) | 0xFF >> ((8 - bitStart) + length - 1);
        data <<= (8 - length);
        data >>= (7 - bitStart);
        b &= mask;
        b |= data;
       while(HAL_I2C_Mem_Write(&MPU6050_IIC_Hard_No, dev, reg, I2C_MEMADD_SIZE_8BIT, &b, 1, 0xff));
    }
    return HAL_OK;
}

/**************************实现函数********************************************
*函数原型:		uint8_t IICwriteBit(uint8_t dev, uint8_t reg, uint8_t bitNum, uint8_t data)
*功　　能:	    读 修改 写 指定设备 指定寄存器一个字节 中的1个位
输入	dev  目标设备地址
		reg	   寄存器地址
		bitNum  要修改目标字节的bitNum位
		data  为0 时，目标位将被清0 否则将被置位
返回   成功 为1 
 		失败为0
*******************************************************************************/ 
uint8_t IICwriteBit(uint8_t dev, uint8_t reg, uint8_t bitNum, uint8_t data)
{
	uint8_t b;
	  while(HAL_I2C_Mem_Read(&MPU6050_IIC_Hard_No, dev, reg, I2C_MEMADD_SIZE_8BIT, &b, 1, 0xff));

	  b = (data != 0) ? (b | (1 << bitNum)) : (b & ~(1 << bitNum));

	  while(HAL_I2C_Mem_Write(&MPU6050_IIC_Hard_No, dev, reg, I2C_MEMADD_SIZE_8BIT, &b, 1, 0xff));
	  return HAL_OK;
}
