/*
 * pfunc_color.h
 *
 *  Created on: Nov 10, 2023
 *      Author: 80424
 */

#ifndef INC_PFUNC_COLOR_H_
#define INC_PFUNC_COLOR_H_
/*颜色设置*/
#define NONE         "\033[0m"

#define RED          "\033[0m\033[1;31m"

#define GREEN        "\033[0m\033[1;32m"

#define BLUE         "\033[0m\033[1;34m"

#define CYAN         "\033[0m\033[1;36m"

#define YELLOW       "\033[0m\033[1;33m"



#endif /* INC_PFUNC_COLOR_H_ */
