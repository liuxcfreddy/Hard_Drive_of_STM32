/*
 * vofa.h
 *
 *  Created on: 2023年7月1日
 *      Author: liuxc
 */

#ifndef USER_INC_VOFA_H_
#define USER_INC_VOFA_H_

#include "main.h"
#include "usart.h"
#include "stdlib.h"
#include "stdio.h"

typedef struct {

		uint8_t UPSOFE_RX_ADDBUFF[4];
	union
	{
	    float f_data;
	    uint8_t UPSOFE_RX_DATABUFF[4];;
	};
}UP_Soft_RX_DataBuff;


/**
 * 这是VOFA的发送JUST FLOAT通信协议数据包
 *
 */
#define CH_COUNT 16	//通道数量

typedef struct{
    float fdata[CH_COUNT];//数据内容
    unsigned char tail[4];//数据包尾标记
}UP_Soft_TX_DataBuff;

//声明时的使用方法
//UP_Soft_TX_DataBuff VOFA_SEND_Struct={
//		.tail={0x00, 0x00, 0x80, 0x7f}//上位机发送数据缓存结构体,并且定义尾帧
//};

void VOFA_SEND(UP_Soft_TX_DataBuff *fframe);

#endif /* USER_INC_VOFA_H_ */
