引脚定义说明：

请在cube mx中将pin user lib名定义为一下

```c
NRF24L01模块的
CE定义为：SPI_CE
CSN定义为：SPI_CSN
IRQ定义为：SPI_IRQ
```

cube mx处理后会有一下效果：



```c
/* Private defines -----------------------------------------------------------*/
#define SPI_IRQ_Pin GPIO_PIN_6
#define SPI_IRQ_GPIO_Port GPIOC
#define SPI_CSN_Pin GPIO_PIN_7
#define SPI_CSN_GPIO_Port GPIOC
#define SPI_CE_Pin GPIO_PIN_8
#define SPI_CE_GPIO_Port GPIOC
#define NRF24L01_SCK_Pin GPIO_PIN_3
#define NRF24L01_SCK_GPIO_Port GPIOB
#define NRF24L01_MISO_Pin GPIO_PIN_4
#define NRF24L01_MISO_GPIO_Port GPIOB
#define NRF24L01_MOSI_Pin GPIO_PIN_5
#define NRF24L01_MOSI_GPIO_Port GPIOB
```

引脚用的是哪一个并不重要，宏定义会处理好；但是一定要在cube mx中对这几个引脚名定义一致



我使用的是SPI1；

如要修改可以自己在底层中修改
